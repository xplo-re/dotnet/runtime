﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Runtime.InteropServices;
using JetBrains.Annotations;


namespace XploRe.Runtime
{

    /// <inheritdoc />
    /// <summary>
    ///     Exception thrown if one of the string arguments provided to a method is empty but the method does not accept
    ///     an empty string argument.
    /// </summary>
    [ComVisible(true)]
    public class ArgumentStringEmptyException : ArgumentException
    {

        private const string DefaultErrorMessage = "String argument must not be empty";

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.ArgumentStringEmptyException" /> instance with the default error message.
        /// </summary>
        public ArgumentStringEmptyException()
            : base(DefaultErrorMessage)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.ArgumentStringEmptyException" /> instance with the default error message
        ///     and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public ArgumentStringEmptyException([CanBeNull] Exception innerException)
            : base(DefaultErrorMessage, innerException)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.ArgumentStringEmptyException" /> instance with the default error message
        ///     and the name of the parameter that causes this exception.
        /// </summary>
        /// <param name="paramName">The name of the parameter that caused the current exception.</param>
        public ArgumentStringEmptyException([CanBeNull] string paramName)
            : base(DefaultErrorMessage, paramName)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.ArgumentStringEmptyException" /> instance with the default error message, 
        ///     the parameter name and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="paramName">The name of the parameter that caused the current exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public ArgumentStringEmptyException([CanBeNull] string paramName, [CanBeNull] Exception innerException)
            : base(DefaultErrorMessage, paramName, innerException)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.ArgumentStringEmptyException" /> instance with a specified error message 
        ///     and the name of the parameter that causes this exception.
        /// </summary>
        /// <param name="paramName">The name of the parameter that caused the current exception.</param>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public ArgumentStringEmptyException(string paramName, string message)
            : base(message, paramName)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.ArgumentStringEmptyException" /> instance with a specified error message, 
        ///     the parameter name and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="paramName">The name of the parameter that caused the current exception.</param>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public ArgumentStringEmptyException(string paramName, string message, Exception innerException)
            : base(message, paramName, innerException)
        {
        }

    }

}
