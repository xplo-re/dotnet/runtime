﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Runtime.InteropServices;
using JetBrains.Annotations;


namespace XploRe.Runtime
{

    /// <inheritdoc />
    /// <summary>
    ///     The exception that is thrown on runtime library errors.
    /// </summary>
    [ComVisible(true)]
    public class RuntimeException : ArgumentNullException
    {

        private const string DefaultMessage = "A runtime library caused a not further specified exception.";

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.RuntimeException" /> instance with the default error 
        ///     message.
        /// </summary>
        public RuntimeException()
            : base(DefaultMessage)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.RuntimeException" /> instance with a specified error 
        ///     message.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public RuntimeException([CanBeNull] string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.RuntimeException" /> instance with a specified error 
        ///     message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">
        ///     The exception that is the cause of the current exception. If the <paramref name="innerException" />
        ///     parameter is not a <c>null</c> reference, then the current exception was raised in a <c>catch</c> block
        ///     that handles the inner exception.
        /// </param>
        public RuntimeException([CanBeNull] string message, [CanBeNull] Exception innerException)
            : base(message, innerException)
        {
        }

    }

}
