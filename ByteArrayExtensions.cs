﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Concurrent;
using System.Linq;
using JetBrains.Annotations;


// ReSharper disable SuggestBaseTypeForParameter

namespace XploRe.Runtime
{

    /// <summary>
    ///     Optimised extension methods for <see cref="byte" /> arrays.
    /// </summary>
    public static class ByteArrayExtensions
    {

        /// <summary>
        ///     Returns a string that represents each <see cref="byte" /> in the array with the corresponding format.
        ///     Optimised for use with the formats "x2" and "X2" to yield a hexadecimal representation of the byte
        ///     array, using lowercase or uppercase hexadecimal characters, respectively.
        /// </summary>
        /// <param name="bytes">This <see cref="byte" /> array to return a string representation for.</param>
        /// <param name="format">Format specification for each <see cref="byte" />.</param>
        /// <returns>
        ///     A string representation of the <see cref="byte" /> array using the specified format for each byte.
        /// </returns>
        public static string ToString([NotNull] this byte[] bytes, [NotNull] string format)
        {
            if (bytes == null) {
                throw new ArgumentNullException(nameof(bytes));
            }

            // ReSharper disable once ConvertIfStatementToSwitchStatement
            if (format == null) {
                throw new ArgumentNullException(nameof(format));
            }

            if (bytes.Length == 0) {
                // Avoid additional computational overhead if byte array is empty.
                return string.Empty;
            }

            if (format == "x2" || format == "X2") {
                // Convert using optimised lookup tables. Only possible due to their fixed character length per byte.
                return ToString(bytes, GetLookupTable(format));
            }

            return string.Join(string.Empty, bytes.Select(b => b.ToString(format)));
        }

        /// <summary>
        ///     Returns a string that represents each <see cref="byte" /> in the array with the corresponding format.
        ///     Optimised for use with the formats "x2" and "X2" to yield a hexadecimal representation of the byte
        ///     array, using lowercase or uppercase hexadecimal characters, respectively.
        /// </summary>
        /// <param name="bytes">This <see cref="byte" /> array to return a string representation for.</param>
        /// <param name="format">Format specification for each <see cref="byte" />.</param>
        /// <param name="delimiter">
        ///     The delimiter <see cref="string" /> inserted between each byte. If <c>null</c>, the delimiter is
        ///     treated equally to an empty string.
        /// </param>
        /// <returns>
        ///     A string representation of the <see cref="byte" /> array using the specified format for each byte,
        ///     delimited by the provided delimiter string.
        /// </returns>
        public static string ToString(
            [NotNull] this byte[] bytes,
            [NotNull] string format,
            [CanBeNull] string delimiter)
        {
            if (string.IsNullOrEmpty(delimiter)) {
                return ToString(bytes, format);
            }

            if (bytes == null) {
                throw new ArgumentNullException(nameof(bytes));
            }

            // ReSharper disable once ConvertIfStatementToSwitchStatement
            if (format == null) {
                throw new ArgumentNullException(nameof(format));
            }

            if (bytes.Length == 0) {
                // Avoid additional computational overhead if byte array is empty.
                return string.Empty;
            }

            if (format == "x2" || format == "X2") {
                // Convert using optimised lookup tables. Only possible due to their fixed character length per byte.
                return ToString(bytes, GetLookupTable(format), delimiter);
            }

            return string.Join(delimiter, bytes.Select(b => b.ToString(format)));
        }

        /// <summary>
        ///     Concurrent storage of lookup tables for fixed-size 2-char formatted values stored as an unsingned int.
        /// </summary>
        [NotNull]
        private static readonly ConcurrentDictionary<string, uint[]> I32LookupTables
            = new ConcurrentDictionary<string, uint[]>();

        /// <summary>
        ///     Yields a lazily initialised conversion table for bytes to their corresponding lowercase hexadecimal 
        ///     string values.
        /// </summary>
        [NotNull]
        private static uint[] GetLookupTable(string format)
        {
            var table = I32LookupTables.GetOrAdd(
                format,
                byteFormat => {
                    var result = new uint[256];

                    for (var i = 0; i < 256; ++i) {
                        var hexByte = i.ToString(byteFormat);

                        if (hexByte == null) {
                            // In case of an invalid format, the ToString() method itself throws a FormatException.
                            throw new RuntimeInconsistencyException(
                                "String conversion of integer with format '{0}' returned null.".FormatWith(byteFormat)
                            );
                        }

                        result[i] = ((uint) hexByte[1] << 16) | hexByte[0];
                    }

                    return result;
                }
            );

            if (table == null) {
                throw new RuntimeInconsistencyException("Concurrent dictionary returned returned null lookup table.");
            }

            return table;
        }

        /// <summary>
        ///     Returns a string representation of the provided byte array using the provided lookup table.
        /// </summary>
        /// <param name="bytes">This non-empty <see cref="byte" /> array to format.</param>
        /// <param name="lookupTable">The 2-character output lookup table to use to convert bytes.</param>
        /// <returns>A continuous string representation of the provided byte array.</returns>
        private static string ToString([NotNull] this byte[] bytes, [NotNull] uint[] lookupTable)
        {
            var bytesLength = bytes.Length;
            var result = new char[bytesLength * 2];

            for (var i = 0; i < bytesLength; ++i) {
                var index = i * 2;
                var value = lookupTable[bytes[i]];

                result[index] = (char) value;
                result[index + 1] = (char) (value >> 16);
            }

            return new string(result);
        }

        /// <summary>
        ///     Returns a string representation of the provided byte array using the provided lookup table.
        /// </summary>
        /// <param name="bytes">This non-empty <see cref="byte" /> array to format.</param>
        /// <param name="lookupTable">The 2-character output lookup table to use to convert bytes.</param>
        /// <param name="delimiter">A non-empty delimiter <see cref="string" /> inserted between each bytes.</param>
        /// <returns>A delimited string representation of the provided byte array.</returns>
        private static string ToString(
            [NotNull] this byte[] bytes,
            [NotNull] uint[] lookupTable,
            [NotNull] string delimiter)
        {
            var bytesLength = bytes.Length;
            var delimiterArray = delimiter.ToCharArray();

            if (delimiterArray == null) {
                throw RuntimeInconsistencyException.FromUnexpectedNull(() => delimiter.ToCharArray());
            }

            var delimiterLength = delimiterArray.Length;
            var result = new char[bytesLength * (2 + delimiterLength) - delimiterLength];

            for (var i = 0; i < bytesLength - 1; ++i) {
                var index = i * (2 + delimiterLength);
                var value = lookupTable[bytes[i]];

                result[index] = (char) value;
                result[index + 1] = (char) (value >> 16);

                // Append delimiter
                Array.Copy(delimiterArray, 0, result, index + 2, delimiterLength);
            }

            // Append last byte.
            var lastValue = lookupTable[bytes[bytesLength - 1]];

            result[result.Length - 2] = (char) lastValue;
            result[result.Length - 1] = (char) (lastValue >> 16);

            return new string(result);
        }

    }

}
