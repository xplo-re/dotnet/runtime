﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Collections.Generic;
using System.ComponentModel;
using FluentAssertions;
using JetBrains.Annotations;
using Xunit;


// ReSharper disable PossibleNullReferenceException

namespace XploRe.Runtime.Tests
{

    public class GenericEnumTest
    {

        public class TestEnumBase<TDerived> : GenericEnum<int, TDerived> where TDerived : TestEnumBase<TDerived>
        {

            protected TestEnumBase(int value) : base(value)
            {
            }

        }

        public class TestEnum : TestEnumBase<TestEnum>
        {

            [Description("Value A")]
            public static readonly TestEnum ValueA = new TestEnum(1);

            [Description("Value B")]
            public static readonly TestEnum ValueB = new TestEnum(2);

            [Description("Value C")]
            public static readonly TestEnum ValueC = new TestEnum(3);

            private TestEnum(int value) : base(value)
            {
            }

        }

        internal static class TestEnumGenerator
        {

            public static IEnumerable<object[]> FieldsWithProperties
            {
                [UsedImplicitly]
                get {
                    yield return new object[] { TestEnum.ValueA, nameof(TestEnum.ValueA), 1, "Value A" };
                    yield return new object[] { TestEnum.ValueB, nameof(TestEnum.ValueB), 2, "Value B" };
                    yield return new object[] { TestEnum.ValueC, nameof(TestEnum.ValueC), 3, "Value C" };
                }
            }

        }

        [Theory]
        [MemberData(nameof(TestEnumGenerator.FieldsWithProperties), MemberType = typeof(TestEnumGenerator))]
        public void FieldsHaveExpectedValueAndName(TestEnum field, string name, int value, string description)
        {
            field.Value.Should().Be(value);
            field.Name.Should().Be(name);
            field.Description.Should().Be(description);
        }

        [Fact]
        public void EnumValuesMatchInOrder()
        {
            TestEnum.Values.Should().ContainInOrder(
                TestEnum.ValueA,
                TestEnum.ValueB,
                TestEnum.ValueC
            );
        }

#pragma warning disable xUnit1026

        [Theory]
        [MemberData(nameof(TestEnumGenerator.FieldsWithProperties), MemberType = typeof(TestEnumGenerator))]
        public void TryConvertMatches(TestEnum field, string name, int value, string description)
        {
            TestEnum.TryConvert(value, out var converted).Should().BeTrue();

            converted.Should().BeSameAs(field);
            converted.Value.Should().Be(value);
            converted.Name.Should().Be(name);
        }

        [Theory]
        [MemberData(nameof(TestEnumGenerator.FieldsWithProperties), MemberType = typeof(TestEnumGenerator))]
        public void ParseMatches(TestEnum field, string name, int value, string description)
        {
            var output = TestEnum.Parse(name);

            output.Should().BeSameAs(field);
            field.Name.Should().Be(name);
            field.Value.Should().Be(value);
        }

#pragma warning restore xUnit1026

    }

}
