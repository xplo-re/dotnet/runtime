﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using FluentAssertions;
using Xunit;


// ReSharper disable PossibleNullReferenceException

namespace XploRe.Runtime.Tests
{

    public class RuntimeInconsistencyExceptionTest
    {

        internal class TestModel
        {

            public string StringValue { get; set; }

            public TestModel GetSelf(bool flag) => flag ? this : null;

        }

        [Fact]
        public void CreateFromUnexpectedNullWithLocalVariable()
        {
            string localString = null;
            var exception = RuntimeInconsistencyException.FromUnexpectedNull(() => localString);

            exception.Inconsistency.Should().Be("localString is null.");
        }

        [Fact]
        public void CreateFromUnexpectedNullWithLocalVariableCall()
        {
            string localString = null;
            var exception = RuntimeInconsistencyException.FromUnexpectedNull(() => localString.Replace("a", "b"));

            exception.Inconsistency.Should().Be(@"localString.Replace(""a"", ""b"") returned null.");
        }

        [Fact]
        public void CreateFromUnexpectedNullWithScopedModelProperty()
        {
            var localModel = new TestModel { StringValue = null };
            var exception = RuntimeInconsistencyException.FromUnexpectedNull(() => localModel.StringValue);

            exception.Inconsistency.Should().Be("localModel.StringValue is null.");
        }

        [Fact]
        public void CreateFromUnexpectedNullWithScopedModelMethodCall()
        {
            var localModel = new TestModel { StringValue = null };
            var exception = RuntimeInconsistencyException.FromUnexpectedNull(() => localModel.GetSelf(true));

            exception.Inconsistency.Should().Be("localModel.GetSelf(True) returned null.");
        }

        [Fact]
        public void CreateFromUnexpectedNullWithScopedModelMethodCallProperty()
        {
            var localModel = new TestModel { StringValue = null };
            var exception =
                RuntimeInconsistencyException.FromUnexpectedNull(() => localModel.GetSelf(true).StringValue);

            exception.Inconsistency.Should().Be("localModel.GetSelf(True).StringValue is null.");
        }

        [Fact]
        public void CreateFromUnexpectedNullWithModelConstructor()
        {
            var exception =
                RuntimeInconsistencyException.FromUnexpectedNull(() => new TestModel { StringValue = null });

            exception.Inconsistency.Should().Be("new TestModel() {StringValue = null} returned null.");
        }

    }

}
