﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using FluentAssertions;
using Xunit;


// ReSharper disable PossibleNullReferenceException

namespace XploRe.Runtime.Tests
{

    public class ByteOrderConverterTest
    {

        [Fact]
        public void ReverseInt16()
        {
            const short original = unchecked((short) 0b10111010_11011111);
            const short expected = unchecked((short) 0b11011111_10111010);

            var reversed = ByteOrderConverter.Swap(original);

            reversed.Should().Be(expected);
        }

        [Fact]
        public void ReverseUInt16()
        {
            const ushort original = (ushort) 0b10111010_11011111;
            const ushort expected = (ushort) 0b11011111_10111010;

            var reversed = ByteOrderConverter.Swap(original);

            reversed.Should().Be(expected);
        }

        [Fact]
        public void ReverseInt32()
        {
            const int original = unchecked((int) 0b10111010_11011111_00100000_01000101);
            const int expected = unchecked((int) 0b01000101_00100000_11011111_10111010);

            var reversed = ByteOrderConverter.Swap(original);

            reversed.Should().Be(expected);
        }

        [Fact]
        public void ReverseUInt32()
        {
            const uint original = 0b10111010_11011111_00100000_01000101;
            const uint expected = 0b01000101_00100000_11011111_10111010;

            var reversed = ByteOrderConverter.Swap(original);

            reversed.Should().Be(expected);
        }

        [Fact]
        public void ReverseInt64()
        {
            const long original =
                unchecked((long) 0b10111010_11011111_00100000_01000101_10101110_11111011_00000100_01010100);
            const long expected =
                unchecked((long) 0b01010100_00000100_11111011_10101110_01000101_00100000_11011111_10111010);

            var reversed = ByteOrderConverter.Swap(original);

            reversed.Should().Be(expected);
        }

        [Fact]
        public void ReverseUInt64()
        {
            const ulong original = 0b10111010_11011111_00100000_01000101_10101110_11111011_00000100_01010100;
            const ulong expected = 0b01010100_00000100_11111011_10101110_01000101_00100000_11011111_10111010;

            var reversed = ByteOrderConverter.Swap(original);

            reversed.Should().Be(expected);
        }

    }

}
