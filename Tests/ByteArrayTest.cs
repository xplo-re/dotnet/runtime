﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Collections.Generic;
using FluentAssertions;
using JetBrains.Annotations;
using Xunit;


// ReSharper disable PossibleNullReferenceException

namespace XploRe.Runtime.Tests
{

    public class ByteArrayTest
    {

        private static readonly byte[] FullByteArray = new byte[] {
            (byte) '\x00', (byte) '\x01', (byte) '\x02', (byte) '\x03', (byte) '\x04', (byte) '\x05', (byte) '\x06',
            (byte) '\x07', (byte) '\x08', (byte) '\x09', (byte) '\x0a', (byte) '\x0b', (byte) '\x0c', (byte) '\x0d',
            (byte) '\x0e', (byte) '\x0f', (byte) '\x10', (byte) '\x11', (byte) '\x12', (byte) '\x13', (byte) '\x14',
            (byte) '\x15', (byte) '\x16', (byte) '\x17', (byte) '\x18', (byte) '\x19', (byte) '\x1a', (byte) '\x1b',
            (byte) '\x1c', (byte) '\x1d', (byte) '\x1e', (byte) '\x1f', (byte) '\x20', (byte) '\x21', (byte) '\x22',
            (byte) '\x23', (byte) '\x24', (byte) '\x25', (byte) '\x26', (byte) '\x27', (byte) '\x28', (byte) '\x29',
            (byte) '\x2a', (byte) '\x2b', (byte) '\x2c', (byte) '\x2d', (byte) '\x2e', (byte) '\x2f', (byte) '\x30',
            (byte) '\x31', (byte) '\x32', (byte) '\x33', (byte) '\x34', (byte) '\x35', (byte) '\x36', (byte) '\x37',
            (byte) '\x38', (byte) '\x39', (byte) '\x3a', (byte) '\x3b', (byte) '\x3c', (byte) '\x3d', (byte) '\x3e',
            (byte) '\x3f', (byte) '\x40', (byte) '\x41', (byte) '\x42', (byte) '\x43', (byte) '\x44', (byte) '\x45',
            (byte) '\x46', (byte) '\x47', (byte) '\x48', (byte) '\x49', (byte) '\x4a', (byte) '\x4b', (byte) '\x4c',
            (byte) '\x4d', (byte) '\x4e', (byte) '\x4f', (byte) '\x50', (byte) '\x51', (byte) '\x52', (byte) '\x53',
            (byte) '\x54', (byte) '\x55', (byte) '\x56', (byte) '\x57', (byte) '\x58', (byte) '\x59', (byte) '\x5a',
            (byte) '\x5b', (byte) '\x5c', (byte) '\x5d', (byte) '\x5e', (byte) '\x5f', (byte) '\x60', (byte) '\x61',
            (byte) '\x62', (byte) '\x63', (byte) '\x64', (byte) '\x65', (byte) '\x66', (byte) '\x67', (byte) '\x68',
            (byte) '\x69', (byte) '\x6a', (byte) '\x6b', (byte) '\x6c', (byte) '\x6d', (byte) '\x6e', (byte) '\x6f',
            (byte) '\x70', (byte) '\x71', (byte) '\x72', (byte) '\x73', (byte) '\x74', (byte) '\x75', (byte) '\x76',
            (byte) '\x77', (byte) '\x78', (byte) '\x79', (byte) '\x7a', (byte) '\x7b', (byte) '\x7c', (byte) '\x7d',
            (byte) '\x7e', (byte) '\x7f', (byte) '\x80', (byte) '\x81', (byte) '\x82', (byte) '\x83', (byte) '\x84',
            (byte) '\x85', (byte) '\x86', (byte) '\x87', (byte) '\x88', (byte) '\x89', (byte) '\x8a', (byte) '\x8b',
            (byte) '\x8c', (byte) '\x8d', (byte) '\x8e', (byte) '\x8f', (byte) '\x90', (byte) '\x91', (byte) '\x92',
            (byte) '\x93', (byte) '\x94', (byte) '\x95', (byte) '\x96', (byte) '\x97', (byte) '\x98', (byte) '\x99',
            (byte) '\x9a', (byte) '\x9b', (byte) '\x9c', (byte) '\x9d', (byte) '\x9e', (byte) '\x9f', (byte) '\xa0',
            (byte) '\xa1', (byte) '\xa2', (byte) '\xa3', (byte) '\xa4', (byte) '\xa5', (byte) '\xa6', (byte) '\xa7',
            (byte) '\xa8', (byte) '\xa9', (byte) '\xaa', (byte) '\xab', (byte) '\xac', (byte) '\xad', (byte) '\xae',
            (byte) '\xaf', (byte) '\xb0', (byte) '\xb1', (byte) '\xb2', (byte) '\xb3', (byte) '\xb4', (byte) '\xb5',
            (byte) '\xb6', (byte) '\xb7', (byte) '\xb8', (byte) '\xb9', (byte) '\xba', (byte) '\xbb', (byte) '\xbc',
            (byte) '\xbd', (byte) '\xbe', (byte) '\xbf', (byte) '\xc0', (byte) '\xc1', (byte) '\xc2', (byte) '\xc3',
            (byte) '\xc4', (byte) '\xc5', (byte) '\xc6', (byte) '\xc7', (byte) '\xc8', (byte) '\xc9', (byte) '\xca',
            (byte) '\xcb', (byte) '\xcc', (byte) '\xcd', (byte) '\xce', (byte) '\xcf', (byte) '\xd0', (byte) '\xd1',
            (byte) '\xd2', (byte) '\xd3', (byte) '\xd4', (byte) '\xd5', (byte) '\xd6', (byte) '\xd7', (byte) '\xd8',
            (byte) '\xd9', (byte) '\xda', (byte) '\xdb', (byte) '\xdc', (byte) '\xdd', (byte) '\xde', (byte) '\xdf',
            (byte) '\xe0', (byte) '\xe1', (byte) '\xe2', (byte) '\xe3', (byte) '\xe4', (byte) '\xe5', (byte) '\xe6',
            (byte) '\xe7', (byte) '\xe8', (byte) '\xe9', (byte) '\xea', (byte) '\xeb', (byte) '\xec', (byte) '\xed',
            (byte) '\xee', (byte) '\xef', (byte) '\xf0', (byte) '\xf1', (byte) '\xf2', (byte) '\xf3', (byte) '\xf4',
            (byte) '\xf5', (byte) '\xf6', (byte) '\xf7', (byte) '\xf8', (byte) '\xf9', (byte) '\xfa', (byte) '\xfb',
            (byte) '\xfc', (byte) '\xfd', (byte) '\xfe', (byte) '\xff'
        };

        private static class Generator
        {

            private const string FullContinuousHexStringLowerCase =
                "000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f202122232425262728292a2b2c2d2e2f3031" +
                "32333435363738393a3b3c3d3e3f404142434445464748494a4b4c4d4e4f505152535455565758595a5b5c5d5e5f60616263" +
                "6465666768696a6b6c6d6e6f707172737475767778797a7b7c7d7e7f808182838485868788898a8b8c8d8e8f909192939495" +
                "969798999a9b9c9d9e9fa0a1a2a3a4a5a6a7a8a9aaabacadaeafb0b1b2b3b4b5b6b7b8b9babbbcbdbebfc0c1c2c3c4c5c6c7" +
                "c8c9cacbcccdcecfd0d1d2d3d4d5d6d7d8d9dadbdcdddedfe0e1e2e3e4e5e6e7e8e9eaebecedeeeff0f1f2f3f4f5f6f7f8f9" +
                "fafbfcfdfeff";

            private const string FullContinuousHexStringUpperCase =
                "000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A2B2C2D2E2F3031" +
                "32333435363738393A3B3C3D3E3F404142434445464748494A4B4C4D4E4F505152535455565758595A5B5C5D5E5F60616263" +
                "6465666768696A6B6C6D6E6F707172737475767778797A7B7C7D7E7F808182838485868788898A8B8C8D8E8F909192939495" +
                "969798999A9B9C9D9E9FA0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7" +
                "C8C9CACBCCCDCECFD0D1D2D3D4D5D6D7D8D9DADBDCDDDEDFE0E1E2E3E4E5E6E7E8E9EAEBECEDEEEFF0F1F2F3F4F5F6F7F8F9" +
                "FAFBFCFDFEFF";

            public static IEnumerable<object[]> ByteArrayToContinous
            {
                [UsedImplicitly]
                get {
                    yield return new object[] { new byte[] { }, "x2", string.Empty };

                    yield return new object[] { FullByteArray, "x2", FullContinuousHexStringLowerCase };
                    yield return new object[] { FullByteArray, "X2", FullContinuousHexStringUpperCase };

                    yield return new object[] {
                        FullByteArray, "D3",
                        "000001002003004005006007008009010011012013014015016017018019020021022023024025026027028029" +
                        "030031032033034035036037038039040041042043044045046047048049050051052053054055056057058059" +
                        "060061062063064065066067068069070071072073074075076077078079080081082083084085086087088089" +
                        "090091092093094095096097098099100101102103104105106107108109110111112113114115116117118119" +
                        "120121122123124125126127128129130131132133134135136137138139140141142143144145146147148149" +
                        "150151152153154155156157158159160161162163164165166167168169170171172173174175176177178179" +
                        "180181182183184185186187188189190191192193194195196197198199200201202203204205206207208209" +
                        "210211212213214215216217218219220221222223224225226227228229230231232233234235236237238239" +
                        "240241242243244245246247248249250251252253254255"
                    };
                }
            }

            public static IEnumerable<object[]> ByteArrayToStringDelimited
            {
                [UsedImplicitly]
                get {
                    yield return new object[] { new byte[] { }, "x2", ",", string.Empty };

                    yield return new object[] {
                        FullByteArray, "x2", " ",
                        "00 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f 10 11 12 13 14 15 16 17 18 19 1a 1b 1c 1d " +
                        "1e 1f 20 21 22 23 24 25 26 27 28 29 2a 2b 2c 2d 2e 2f 30 31 32 33 34 35 36 37 38 39 3a 3b " +
                        "3c 3d 3e 3f 40 41 42 43 44 45 46 47 48 49 4a 4b 4c 4d 4e 4f 50 51 52 53 54 55 56 57 58 59 " +
                        "5a 5b 5c 5d 5e 5f 60 61 62 63 64 65 66 67 68 69 6a 6b 6c 6d 6e 6f 70 71 72 73 74 75 76 77 " +
                        "78 79 7a 7b 7c 7d 7e 7f 80 81 82 83 84 85 86 87 88 89 8a 8b 8c 8d 8e 8f 90 91 92 93 94 95 " +
                        "96 97 98 99 9a 9b 9c 9d 9e 9f a0 a1 a2 a3 a4 a5 a6 a7 a8 a9 aa ab ac ad ae af b0 b1 b2 b3 " +
                        "b4 b5 b6 b7 b8 b9 ba bb bc bd be bf c0 c1 c2 c3 c4 c5 c6 c7 c8 c9 ca cb cc cd ce cf d0 d1 " +
                        "d2 d3 d4 d5 d6 d7 d8 d9 da db dc dd de df e0 e1 e2 e3 e4 e5 e6 e7 e8 e9 ea eb ec ed ee ef " +
                        "f0 f1 f2 f3 f4 f5 f6 f7 f8 f9 fa fb fc fd fe ff"
                    };
                    yield return new object[] {
                        FullByteArray, "x2", ",",
                        "00,01,02,03,04,05,06,07,08,09,0a,0b,0c,0d,0e,0f,10,11,12,13,14,15,16,17,18,19,1a,1b,1c,1d," +
                        "1e,1f,20,21,22,23,24,25,26,27,28,29,2a,2b,2c,2d,2e,2f,30,31,32,33,34,35,36,37,38,39,3a,3b," +
                        "3c,3d,3e,3f,40,41,42,43,44,45,46,47,48,49,4a,4b,4c,4d,4e,4f,50,51,52,53,54,55,56,57,58,59," +
                        "5a,5b,5c,5d,5e,5f,60,61,62,63,64,65,66,67,68,69,6a,6b,6c,6d,6e,6f,70,71,72,73,74,75,76,77," +
                        "78,79,7a,7b,7c,7d,7e,7f,80,81,82,83,84,85,86,87,88,89,8a,8b,8c,8d,8e,8f,90,91,92,93,94,95," +
                        "96,97,98,99,9a,9b,9c,9d,9e,9f,a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,aa,ab,ac,ad,ae,af,b0,b1,b2,b3," +
                        "b4,b5,b6,b7,b8,b9,ba,bb,bc,bd,be,bf,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,ca,cb,cc,cd,ce,cf,d0,d1," +
                        "d2,d3,d4,d5,d6,d7,d8,d9,da,db,dc,dd,de,df,e0,e1,e2,e3,e4,e5,e6,e7,e8,e9,ea,eb,ec,ed,ee,ef," +
                        "f0,f1,f2,f3,f4,f5,f6,f7,f8,f9,fa,fb,fc,fd,fe,ff"
                    };
                    yield return new object[] {
                        FullByteArray, "x2", "-:-",
                        "00-:-01-:-02-:-03-:-04-:-05-:-06-:-07-:-08-:-09-:-0a-:-0b-:-0c-:-0d-:-0e-:-0f-:-10-:-11-:-" +
                        "12-:-13-:-14-:-15-:-16-:-17-:-18-:-19-:-1a-:-1b-:-1c-:-1d-:-1e-:-1f-:-20-:-21-:-22-:-23-:-" +
                        "24-:-25-:-26-:-27-:-28-:-29-:-2a-:-2b-:-2c-:-2d-:-2e-:-2f-:-30-:-31-:-32-:-33-:-34-:-35-:-" +
                        "36-:-37-:-38-:-39-:-3a-:-3b-:-3c-:-3d-:-3e-:-3f-:-40-:-41-:-42-:-43-:-44-:-45-:-46-:-47-:-" +
                        "48-:-49-:-4a-:-4b-:-4c-:-4d-:-4e-:-4f-:-50-:-51-:-52-:-53-:-54-:-55-:-56-:-57-:-58-:-59-:-" +
                        "5a-:-5b-:-5c-:-5d-:-5e-:-5f-:-60-:-61-:-62-:-63-:-64-:-65-:-66-:-67-:-68-:-69-:-6a-:-6b-:-" +
                        "6c-:-6d-:-6e-:-6f-:-70-:-71-:-72-:-73-:-74-:-75-:-76-:-77-:-78-:-79-:-7a-:-7b-:-7c-:-7d-:-" +
                        "7e-:-7f-:-80-:-81-:-82-:-83-:-84-:-85-:-86-:-87-:-88-:-89-:-8a-:-8b-:-8c-:-8d-:-8e-:-8f-:-" +
                        "90-:-91-:-92-:-93-:-94-:-95-:-96-:-97-:-98-:-99-:-9a-:-9b-:-9c-:-9d-:-9e-:-9f-:-a0-:-a1-:-" +
                        "a2-:-a3-:-a4-:-a5-:-a6-:-a7-:-a8-:-a9-:-aa-:-ab-:-ac-:-ad-:-ae-:-af-:-b0-:-b1-:-b2-:-b3-:-" +
                        "b4-:-b5-:-b6-:-b7-:-b8-:-b9-:-ba-:-bb-:-bc-:-bd-:-be-:-bf-:-c0-:-c1-:-c2-:-c3-:-c4-:-c5-:-" +
                        "c6-:-c7-:-c8-:-c9-:-ca-:-cb-:-cc-:-cd-:-ce-:-cf-:-d0-:-d1-:-d2-:-d3-:-d4-:-d5-:-d6-:-d7-:-" +
                        "d8-:-d9-:-da-:-db-:-dc-:-dd-:-de-:-df-:-e0-:-e1-:-e2-:-e3-:-e4-:-e5-:-e6-:-e7-:-e8-:-e9-:-" +
                        "ea-:-eb-:-ec-:-ed-:-ee-:-ef-:-f0-:-f1-:-f2-:-f3-:-f4-:-f5-:-f6-:-f7-:-f8-:-f9-:-fa-:-fb-:-" +
                        "fc-:-fd-:-fe-:-ff"
                    };
                    yield return new object[] {
                        FullByteArray, "D", " ",
                        "0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 " +
                        "34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 " +
                        "64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 " +
                        "94 95 96 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 " +
                        "118 119 120 121 122 123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 139 140 " +
                        "141 142 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 160 161 162 163 " +
                        "164 165 166 167 168 169 170 171 172 173 174 175 176 177 178 179 180 181 182 183 184 185 186 " +
                        "187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 " +
                        "210 211 212 213 214 215 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 " +
                        "233 234 235 236 237 238 239 240 241 242 243 244 245 246 247 248 249 250 251 252 253 254 255"
                    };

                    // With an empty or missing delimiter, result shoul be same as continuous version.
                    yield return new object[] {
                        FullByteArray, "x2", null,
                        FullContinuousHexStringLowerCase
                    };
                    yield return new object[] {
                        FullByteArray, "x2", string.Empty,
                        FullContinuousHexStringLowerCase
                    };
                }
            }

        }

        [Theory]
        [MemberData(nameof(Generator.ByteArrayToContinous), MemberType = typeof(Generator))]
        public void ByteArrayToContinousHexTest(
            [NotNull] byte[] bytes,
            [NotNull] string format,
            [NotNull] string expectedOutput)
        {
            var output = bytes.ToString(format);

            output.Should().NotBeNull();
            output.Should().Be(expectedOutput);
        }

        [Theory]
        [MemberData(nameof(Generator.ByteArrayToStringDelimited), MemberType = typeof(Generator))]
        public void ByteArrayToStringDelimitedHexTest(
            [NotNull] byte[] bytes,
            [NotNull] string format,
            [CanBeNull] string delimiter,
            [NotNull] string expectedOutput)
        {
            var output = bytes.ToString(format, delimiter);

            output.Should().NotBeNull();
            output.Should().Be(expectedOutput);
        }

    }

}
