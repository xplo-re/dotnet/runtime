﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

namespace XploRe.Runtime
{

    /// <summary>
    ///     Byte-order variants.
    /// </summary>
    public enum ByteOrder
    {

        /// <summary>
        ///     The byte-order of the host.
        /// </summary>
        Host = 0,

        /// <summary>
        ///     Big-endian byte-order.
        /// </summary>
        BigEndian = 1,

        /// <summary>
        ///     Little-endian byte-order.
        /// </summary>
        LittleEndian = 2

    }

}
