﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Runtime.CompilerServices;


[assembly: InternalsVisibleTo("XploRe.AspNetCore.Authorization")]
[assembly: InternalsVisibleTo("XploRe.AspNetCore.Identity")]
[assembly: InternalsVisibleTo("XploRe.AspNetCore.Mvc")]
[assembly: InternalsVisibleTo("XploRe.AspNetCore.Mvc.TagHelpers")]
[assembly: InternalsVisibleTo("XploRe.AspNetCore.Mvc.TagHelpers.Bootstrap4")]
[assembly: InternalsVisibleTo("XploRe.AspNetCore.Mvc.TagHelpers.FontAwesome")]
[assembly: InternalsVisibleTo("XploRe.Collections")]
[assembly: InternalsVisibleTo("XploRe.Configuration.Yaml")]
[assembly: InternalsVisibleTo("XploRe.Configuration.Yaml.Tests")]
[assembly: InternalsVisibleTo("XploRe.Diagnostics")]
[assembly: InternalsVisibleTo("XploRe.Diagnostics.Contracts")]
[assembly: InternalsVisibleTo("XploRe.Drawing")]
[assembly: InternalsVisibleTo("XploRe.Json")]
[assembly: InternalsVisibleTo("XploRe.Json.Tests")]
[assembly: InternalsVisibleTo("XploRe.Linq.Expressions")]
[assembly: InternalsVisibleTo("XploRe.Logging")]
[assembly: InternalsVisibleTo("XploRe.Logging.Serilog")]
[assembly: InternalsVisibleTo("XploRe.Net.JsonRpc")]
[assembly: InternalsVisibleTo("XploRe.Net.JsonRpc.Tests")]
[assembly: InternalsVisibleTo("XploRe.Reflection")]
[assembly: InternalsVisibleTo("XploRe.Runtime.Loader")]
[assembly: InternalsVisibleTo("XploRe.Runtime.Tests")]
[assembly: InternalsVisibleTo("XploRe.Security")]
[assembly: InternalsVisibleTo("XploRe.Util.Uuid")]
[assembly: InternalsVisibleTo("XploRe.Util.Uuid.Tests")]
