﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Runtime.CompilerServices;


namespace XploRe.Runtime
{

    /// <summary>
    ///     Extends <see cref="ByteOrder" /> with support methods.
    /// </summary>
    public static class ByteOrderExtensions
    {

        /// <summary>
        ///     Determines whether the <see cref="ByteOrder" /> enumeration value reflects the byte-order of the host.
        /// </summary>
        /// <param name="order">This <see cref="ByteOrder" /> enumeration value to test.</param>
        /// <returns>
        ///     <c>true</c>, if <paramref name="order" /> is equal to the byte-order of the host, otherwise <c>false</c>.
        /// </returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsHostOrder(this ByteOrder order)
        {
            if (order == ByteOrder.Host) {
                return true;
            }

            if (BitConverter.IsLittleEndian) {
                return order == ByteOrder.LittleEndian;
            }

            // Big endian host.
            return order == ByteOrder.BigEndian;
        }

        /// <summary>
        ///     Determines whether the <see cref="ByteOrder" /> enumeration value reflects the network byte-order, i.e.
        ///     <see cref="ByteOrder.BigEndian" />.
        /// </summary>
        /// <param name="order">This <see cref="ByteOrder" /> enumeration value to test.</param>
        /// <returns>
        ///     <c>true</c>, if <paramref name="order" /> is equal to the network byte-order, i.e. describes
        ///     <see cref="ByteOrder.BigEndian"/>, otherwise <c>false</c>.
        /// </returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsNetworkOrder(this ByteOrder order)
        {
            if (order == ByteOrder.Host) {
                return ByteOrder.BigEndian.IsHostOrder();
            }

            return order == ByteOrder.BigEndian;
        }

    }

}
