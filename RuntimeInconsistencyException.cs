﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using JetBrains.Annotations;
using XploRe.Runtime.Internal;


namespace XploRe.Runtime
{

    /// <inheritdoc />
    /// <summary>
    ///     The exception that is thrown when an inconsistency in a runtime library has been detected.
    /// </summary>
    [ComVisible(true)]
    public class RuntimeInconsistencyException : RuntimeException
    {

        internal const string DefaultMessage = "A runtime library inconsistency was detected.";

        // Use explicit private member to avoid virtual property setter calls in constructors.
        private readonly string _inconsistency;

        // ReSharper disable once ConvertToAutoProperty
        /// <summary>
        ///     Technical description of the detected inconsistency.
        /// </summary>
        [CanBeNull]
        public virtual string Inconsistency => _inconsistency;

        /// <inheritdoc />
        /// <summary>
        ///     Gets the error message including the technical description on the detected inconsistency, or only the
        ///     error message if no inconsistency description is set.
        /// </summary>
        [CanBeNull]
        public override string Message
        {
            get {
                var message = base.Message;

                if (string.IsNullOrEmpty(Inconsistency)) {
                    return message;
                }

                return message + Environment.NewLine + "Detected inconsistency: " + Inconsistency;
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.RuntimeInconsistencyException" /> instance with the 
        ///     default error message.
        /// </summary>
        public RuntimeInconsistencyException()
            : base(DefaultMessage)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.RuntimeInconsistencyException" /> instance with a 
        ///     technical description on the detected runtime library inconsistency.
        /// </summary>
        /// <param name="inconsistency">The technical description on the detected runtime library inconsistency.</param>
        public RuntimeInconsistencyException([CanBeNull] string inconsistency)
            : base(DefaultMessage)
        {
            _inconsistency = inconsistency;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.RuntimeInconsistencyException" /> instance with a 
        ///     technical description on the detected runtime library inconsistency and an error message.
        /// </summary>
        /// <param name="inconsistency">The technical description on the detected runtime library inconsistency.</param>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public RuntimeInconsistencyException([CanBeNull] string inconsistency, [CanBeNull] string message)
            : base(message)
        {
            _inconsistency = inconsistency;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.RuntimeInconsistencyException" /> instance with a 
        ///     specified error message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">
        ///     The exception that is the cause of the current exception. If the <paramref name="innerException" />
        ///     parameter is not a <c>null</c> reference, then the current exception was raised in a <c>catch</c> block
        ///     that handles the inner exception.
        /// </param>
        public RuntimeInconsistencyException([CanBeNull] string message, [CanBeNull] Exception innerException)
            : base(message, innerException)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="T:XploRe.Runtime.RuntimeInconsistencyException" /> instance with a 
        ///     specified error message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="inconsistency">The technical description on the detected runtime library inconsistency.</param>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">
        ///     The exception that is the cause of the current exception. If the <paramref name="innerException" />
        ///     parameter is not a <c>null</c> reference, then the current exception was raised in a <c>catch</c> block
        ///     that handles the inner exception.
        /// </param>
        public RuntimeInconsistencyException(
            [CanBeNull] string inconsistency,
            [CanBeNull] string message,
            [CanBeNull] Exception innerException)
            : base(message, innerException)
        {
            _inconsistency = inconsistency;
        }


        #region Factories

        /// <summary>
        ///     Creates a new <see cref="RuntimeInconsistencyException" /> instance for an unexpected <c>null</c> value.
        /// </summary>
        /// <param name="expression">The expression that yielded <c>null</c>.</param>
        /// <returns>A new initialised <see cref="RuntimeInconsistencyException" /> instance.</returns>
        [NotNull]
        public static RuntimeInconsistencyException FromUnexpectedNull<T>([NotNull] Expression<Func<T>> expression)
        {
            if (expression == null) {
                throw new ArgumentNullException(nameof(expression));
            }

            var verb = "is";

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (expression.Body?.NodeType) {
            case ExpressionType.Call:
            case ExpressionType.Invoke:
            case ExpressionType.MemberInit: // Constructor call.
                verb = "returned";
                break;
            }

            var formatter = new ExpressionFormatter<T>(expression);

            return new RuntimeInconsistencyException($"{formatter} {verb} null.");
        }

        #endregion

    }

}
