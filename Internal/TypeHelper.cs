﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;


namespace XploRe.Runtime.Internal
{

    internal static class TypeHelper
    {

        /// <summary>
        ///     Determines whether the <see cref="CompilerGeneratedAttribute" /> is set for a given type.
        /// </summary>
        /// <param name="type">The <see cref="Type" /> to check.</param>
        /// <returns>
        ///     <c>true</c>, if the <see cref="CompilerGeneratedAttribute" /> is set for <paramref name="type" />,
        ///     otherwise <c>false</c>.
        /// </returns>
        public static bool IsCompilerGenerated([NotNull] Type type)
        {
            return type.GetTypeInfo()?.GetCustomAttribute<CompilerGeneratedAttribute>() != null;
        }

        /// <summary>
        ///     Determines whether a given type is a compiler-generated closure that should be treated transparently.
        /// </summary>
        /// <param name="type">The <see cref="Type" /> to check.</param>
        /// <returns>
        ///     <c>true</c>, if <paramref name="type" /> is a compiler-generated closure that should be treated
        ///     transparently, otherwise <c>false</c>.
        /// </returns>
        public static bool IsCompilerGeneratedDisplayClass([NotNull] Type type)
        {
            return IsCompilerGenerated(type) && type.Name?.StartsWith("<>c__DisplayClass") == true;
        }

    }

}
