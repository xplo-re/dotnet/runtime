﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Linq.Expressions;
using System.Reflection;
using JetBrains.Annotations;


namespace XploRe.Runtime.Internal
{

    /// <summary>
    ///     Formats an expression for use within error messages by replacing closure wrappers by their actual local
    ///     variable name for improved readability.
    /// </summary>
    /// <typeparam name="T">Lambda expression type.</typeparam>
    internal class ExpressionFormatter<T>
    {

        /// <summary>
        ///     The <see cref="Expression{TDelegate}" /> this formatter was initialised with.
        /// </summary>
        [NotNull]
        public Expression<Func<T>> Expression { get; }

        /// <summary>
        ///     Initialises a new <see cref="ExpressionFormatter{T}" /> instance with a given expression.
        /// </summary>
        /// <param name="expression">The <see cref="Expression{T}" /> to format.</param>
        public ExpressionFormatter([NotNull] Expression<Func<T>> expression)
        {
            Expression = expression;
        }

        /// <summary>
        ///     Returns the formatted expression.
        /// </summary>
        /// <returns>The formatted expression.</returns>
        public override string ToString()
        {
            var visitor = new ExpressionFormatterVisitor();
            var body = Expression.Body;

            if (body == null) {
                return string.Empty;
            }

            return visitor.Modify(body).ToString();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Pre-processes an expression tree by replacing closure leaf nodes by their respective local variable name.
        /// </summary>
        private class ExpressionFormatterVisitor : ExpressionVisitor
        {

            /// <summary>
            ///     Modifies a given expression.
            /// </summary>
            /// <param name="node">Expression node to modify.</param>
            /// <returns>The modified expression.</returns>
            [NotNull]
            public Expression Modify([NotNull] Expression node)
            {
                return Visit(node)
                       ?? throw new RuntimeInconsistencyException("Expression tree traversal returned null.");
            }

            protected override Expression VisitMember(MemberExpression node)
            {
                // Replace the closure with a local variable node for much improved readabilty; output will look the
                // same as the original code. The closure container is a constant expression with member access.
                if (node?.NodeType == ExpressionType.MemberAccess && node.Expression is ConstantExpression constant) {
                    // The closure is represented by a compiler-generated "display class". Extract the corresponding 
                    // field that captured the local variable.
                    if (constant.Type != null &&
                        TypeHelper.IsCompilerGeneratedDisplayClass(constant.Type) &&
                        node.Member is FieldInfo field) {
                        // Expose the closure field as a local variable.
                        return System.Linq.Expressions.Expression.Variable(field.FieldType, field.Name);
                    }
                }

                return base.VisitMember(node);
            }

        }

    }

}
