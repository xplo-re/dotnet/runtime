﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.IO;
using System.Text;
using JetBrains.Annotations;


namespace XploRe.Runtime
{

    /// <summary>
    ///     Provides convenience extensions for <see cref="string" /> instances.
    /// </summary>
    public static class StringExtensions
    {

        /// <summary>
        ///     Determines whether the end of this string instance matches the specified string instance based on a
        ///     case-sensitive comparison, optionally taking the current culture into account.
        /// </summary>
        /// <param name="self">This <see cref="string" /> instance whose end to compare to another.</param>
        /// <param name="value">
        ///     The <see cref="string" /> instance to compare to the substring at the end of this instance.
        /// </param>
        /// <param name="useCurrentCulture">
        ///     If set to <c>true</c>, the current culture will be taken into account, otherwise the strings are
        ///     compared ordinally.
        /// </param>
        /// <returns>
        ///     <c>true</c>, if the <paramref name="value" /> parameter matches the end of this <see cref="string" />,
        ///     otherwise <c>false</c>.
        /// </returns>
        [Pure]
        public static bool EndsWith([NotNull] this string self, [NotNull] string value, bool useCurrentCulture)
        {
            return self.EndsWith(
                value,
                useCurrentCulture
                    ? StringComparison.CurrentCulture
                    : StringComparison.Ordinal
            );
        }

        /// <summary>
        ///     Determines whether the end of this string instance matches the specified string instance based on a
        ///     case-insensitive comparison, taking the current culture into account.
        /// </summary>
        /// <param name="self">This <see cref="string" /> instance whose end to compare to another.</param>
        /// <param name="value">
        ///     The <see cref="string" /> instance to compare to the substring at the end of this instance.
        /// </param>
        /// <returns>
        ///     <c>true</c>, if the <paramref name="value" /> parameter matches the end of this <see cref="string" />,
        ///     otherwise <c>false</c>.
        /// </returns>
        [Pure]
        public static bool EndsWithIgnoreCase([NotNull] this string self, [NotNull] string value)
        {
            return self.EndsWith(value, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        ///     Determines whether the end of this string instance matches the specified string instance based on a
        ///     case-insensitive comparison, optionally taking the current culture into account.
        /// </summary>
        /// <param name="self">This <see cref="string" /> instance whose end to compare to another.</param>
        /// <param name="value">
        ///     The <see cref="string" /> instance to compare to the substring at the end of this instance.
        /// </param>
        /// <param name="useCurrentCulture">
        ///     If set to <c>true</c>, the current culture will be taken into account, otherwise the strings are
        ///     compared ordinally.
        /// </param>
        /// <returns>
        ///     <c>true</c>, if the <paramref name="value" /> parameter matches the end of this <see cref="string" />,
        ///     otherwise <c>false</c>.
        /// </returns>
        [Pure]
        public static bool EndsWithIgnoreCase(
            [NotNull] this string self,
            [NotNull] string value,
            bool useCurrentCulture)
        {
            return self.EndsWith(
                value,
                useCurrentCulture
                    ? StringComparison.CurrentCultureIgnoreCase
                    : StringComparison.OrdinalIgnoreCase
            );
        }

        /// <summary>
        ///     Determines whether two <see cref="string" /> instances have the same value based on a case-sensitive
        ///     comparison, optionally taking the current culture into account.
        /// </summary>
        /// <param name="self">This <see cref="string" /> instance to compare with another. Can be <c>null</c>.</param>
        /// <param name="other">The <see cref="string" /> instance to compare with. Can be <c>null</c>.</param>
        /// <param name="useCurrentCulture">
        ///     If set to <c>true</c>, the current culture will be taken into account, otherwise the strings are
        ///     compared ordinally.
        /// </param>
        /// <returns><c>true</c>, if both strings are equal w.r.t. the culture flag.</returns>
        [Pure]
        public static bool Equals([CanBeNull] this string self, [CanBeNull] string other, bool useCurrentCulture)
        {
            return string.Equals(
                self, other,
                useCurrentCulture
                    ? StringComparison.CurrentCulture
                    : StringComparison.Ordinal
            );
        }

        /// <summary>
        ///     Determines whether two <see cref="string" /> instances have the same value based on a case-insensitive,
        ///     ordinal comparison.
        /// </summary>
        /// <param name="self">The <see cref="string" /> instance to compare with another. Can be <c>null</c>/</param>
        /// <param name="other">The <see cref="string" /> instance to compare with. Can be <c>null</c>.</param>
        /// <returns>
        ///     <c>true</c>, if both strings are equal ignoring case without taking the current culture into account.
        /// </returns>
        [Pure]
        public static bool EqualsIgnoreCase([CanBeNull] this string self, [CanBeNull] string other)
        {
            return string.Equals(self, other, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        ///     Determines whether two <see cref="string" /> instances have the same value based on a case-insensitive
        ///     comparison, optionally taking the current culture into account.
        /// </summary>
        /// <param name="self">The <see cref="string" /> instance to compare with another. Can be <c>null</c>/</param>
        /// <param name="other">The <see cref="string" /> instance to compare with. Can be <c>null</c>.</param>
        /// <param name="useCurrentCulture">
        ///     If set to <c>true</c>, the current culture will be taken into account, otherwise the strings are
        ///     compared ordinally.
        /// </param>
        /// <returns><c>true</c>, if both strings are equal ignoring case w.r.t. the culture flag.</returns>
        [Pure]
        public static bool EqualsIgnoreCase(
            [CanBeNull] this string self,
            [CanBeNull] string other,
            bool useCurrentCulture)
        {
            return string.Equals(
                self, other,
                useCurrentCulture
                    ? StringComparison.CurrentCultureIgnoreCase
                    : StringComparison.OrdinalIgnoreCase
            );
        }

        /// <summary>
        ///     Replaces all positional format placeholders in the current string with the string representation of
        ///     their corresponding object in the arguments array.  
        /// </summary>
        /// <param name="format">This composite format string instance.</param>
        /// <param name="args">An object array with all formatting arguments.</param>
        /// <returns>
        ///     A copy of the current <paramref name="format" /> string instance that has all positional format
        ///     placeholders replaced by the string representations of their corresponding objects in the
        ///     <paramref name="args" /> parameters array.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     The <paramref name="format" /> or <paramref name="args" /> parameter is <c>null</c>.
        /// </exception>
        /// <exception cref="FormatException">
        ///     The format of the current string is invalid; or the index of a positional format placeholder is negative
        ///     or greater than or equal to the length of the <paramref name="args" /> parameter array.
        /// </exception>
        [Pure]
        [StringFormatMethod("format")]
        public static string FormatWith([NotNull] this string format, [NotNull] [ItemCanBeNull] params object[] args)
        {
            if (format == null) {
                throw new ArgumentNullException(nameof(format));
            }

            if (args == null) {
                throw new ArgumentNullException(nameof(args));
            }

            return string.Format(format, args);
        }

        /// <summary>
        ///     Determines whether the beginning of this string instance matches the specified string instance based on 
        ///     a case-sensitive comparison, optionally taking the current culture into account.
        /// </summary>
        /// <param name="self">This <see cref="string" /> instance whose beginning to compare to another.</param>
        /// <param name="value">
        ///     The <see cref="string" /> instance to compare to the substring at the beginning of this instance.
        /// </param>
        /// <param name="useCurrentCulture">
        ///     If set to <c>true</c>, the current culture will be taken into account, otherwise the strings are
        ///     compared ordinally.
        /// </param>
        /// <returns>
        ///     <c>true</c>, if the <paramref name="value" /> parameter matches the beginning of this
        ///     <see cref="string" />, otherwise <c>false</c>.
        /// </returns>
        [Pure]
        public static bool StartsWith([NotNull] this string self, [NotNull] string value, bool useCurrentCulture)
        {
            return self.StartsWith(
                value,
                useCurrentCulture
                    ? StringComparison.CurrentCulture
                    : StringComparison.Ordinal
            );
        }

        /// <summary>
        ///     Determines whether the beginning of this string instance matches the specified string instance based on 
        ///     a case-insensitive comparison, taking the current culture into account.
        /// </summary>
        /// <param name="self">This <see cref="string" /> instance whose end to compare to another.</param>
        /// <param name="value">
        ///     The <see cref="string" /> instance to compare to the substring at the beginning of this instance.
        /// </param>
        /// <returns>
        ///     <c>true</c>, if the <paramref name="value" /> parameter matches the beginning of this 
        ///     <see cref="string" />, otherwise <c>false</c>.
        /// </returns>
        [Pure]
        public static bool StartsWithIgnoreCase([NotNull] this string self, [NotNull] string value)
        {
            return self.StartsWith(value, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        ///     Determines whether the beginning of this string instance matches the specified string instance based on
        ///     a case-insensitive comparison, optionally taking the current culture into account.
        /// </summary>
        /// <param name="self">This <see cref="string" /> instance whose beginning to compare to another.</param>
        /// <param name="value">
        ///     The <see cref="string" /> instance to compare to the substring at the beginning of this instance.
        /// </param>
        /// <param name="useCurrentCulture">
        ///     If set to <c>true</c>, the current culture will be taken into account, otherwise the strings are
        ///     compared ordinally.
        /// </param>
        /// <returns>
        ///     <c>true</c>, if the <paramref name="value" /> parameter matches the beginning of this
        ///     <see cref="string" />, otherwise <c>false</c>.
        /// </returns>
        [Pure]
        public static bool StartsWithIgnoreCase(
            [NotNull] this string self,
            [NotNull] string value,
            bool useCurrentCulture)
        {
            return self.StartsWith(
                value,
                useCurrentCulture
                    ? StringComparison.CurrentCultureIgnoreCase
                    : StringComparison.OrdinalIgnoreCase
            );
        }

        /// <summary>
        ///     Returns a new <see cref="MemoryStream" /> from this string using the given encoding. 
        /// </summary>
        /// <param name="self">This <see cref="string" /> instance to return a <see cref="MemoryStream" /> for.</param>
        /// <param name="encoding">The <see cref="Encoding" /> used to convert the string into a byte sequence.</param>
        /// <returns>A new <see cref="MemoryStream" /> instance over the encoded string contents.</returns>
        [Pure]
        [NotNull]
        public static MemoryStream ToMemoryStream([NotNull] this string self, [NotNull] Encoding encoding)
        {
            if (self == null) {
                throw new ArgumentNullException(nameof(self));
            }

            if (encoding == null) {
                throw new ArgumentNullException(nameof(encoding));
            }

            var bytes = encoding.GetBytes(self);

            return new MemoryStream(bytes);
        }

    }

}
