﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;


namespace XploRe.Runtime
{

    // ReSharper disable once InheritdocConsiderUsage
    /// <summary>
    /// <para>
    ///     Basic enum implementation for a generic class that may provide additional properties, methods and implicit
    ///     conversion operators as needed.
    /// </para>
    /// <para>
    ///     Each enum member must be defined as public, static fields, initialised with an instance of the enum with the
    ///     value they are supposed to represent. Optionally, the <see cref="T:System.ComponentModel.DescriptionAttribute" /> 
    ///     attribute may be used to set a description.
    ///     At runtime, the name of each member is automatically set to the name of the corresponding field and the
    ///     description field it set to the value of the <see cref="T:System.ComponentModel.DescriptionAttribute" />, if 
    ///     any.
    /// </para>
    /// <para>
    ///     An <see cref="T:System.ArgumentException" /> is thrown during static initialisation if two members use the 
    ///     same value.
    /// </para>
    /// </summary>
    /// <typeparam name="TValue">
    ///     Type of enumeration value. Must be a value type that supports comparison and equality.
    /// </typeparam>
    /// <typeparam name="TDerived">
    ///     The type of the derived class of the <see cref="T:XploRe.Runtime.GenericEnum`2" />.
    /// </typeparam>
    public abstract class GenericEnum<TValue, TDerived>
        : IComparable, IComparable<TDerived>, IComparer<TDerived>, IEquatable<TDerived>
        where TValue : struct, IComparable<TValue>, IEquatable<TValue>
        where TDerived : GenericEnum<TValue, TDerived>
    {

        /// <summary>
        ///     Value of enumeration member.
        /// </summary>
        public TValue Value { get; }

        /// <summary>
        ///     Optional description of enumeration member. Can be set via the <see cref="DescriptionAttribute" />
        ///     attribute.
        /// </summary>
        [CanBeNull]
        public string Description => _description.Value;

        /// <summary>
        ///     Name of enumeration member.
        /// </summary>
        [NotNull]
        public string Name
        {
            get {
                var name = _name.Value;

                if (name == null) {
                    throw new RuntimeInconsistencyException(
                        "{0} property of {1} wrapper returned null.".FormatWith(nameof(_name.Value), _name.GetType())
                    );
                }

                return name;
            }
        }

        /// <summary>
        ///     Initialises a new <see cref="GenericEnum{TValue,TDerived} "/> with a given value.
        /// </summary>
        /// <param name="value">Value to initialise new enum member with.</param>
        protected GenericEnum(TValue value)
        {
            Value = value;
            Members.Add(value, (TDerived) this);

            // Resolve description lazily when accessed.
            _description = new Lazy<string>(
                () => MemberFieldMappings.First(mapping => ReferenceEquals(this, mapping.Instance))?.Description
            );

            // Resolve name lazily when accessed.
            _name = new Lazy<string>(
                () => MemberFieldMappings.First(mapping => ReferenceEquals(this, mapping.Instance))?.Name
            );
        }


        #region Enum Members

        /// <summary>
        ///     List over all enum member values of the enum type.
        /// </summary>
        [NotNull]
        public static IEnumerable<TDerived> Values
        {
            get {
                var values = Members.Values;

                if (values == null) {
                    throw new RuntimeInconsistencyException(
                        "List property {0} is null for enum property {1}.".FormatWith(
                            nameof(Members.Values),
                            nameof(Members)
                        )
                    );
                }

                return values;
            }
        }

        /// <summary>
        ///     Yields the first member with a matching name.
        /// </summary>
        /// <param name="name">Name of member to search for. Member names comparison is case-insensitive.</param>
        /// <returns>
        ///     First matching enumeration member with the given name or <c>null</c>, if no match was found.
        /// </returns>
        [CanBeNull]
        public static TDerived Parse([CanBeNull] string name)
        {
            // Base search on the member field mapping. This is the only field guaranteed to be initialised. If the enum
            // has not been referenced before, static members may not have been initialised yet (this seems to occur in
            // dynamically generated code such as ASP.NET MVC Razor views).
            // Members are automatically initialised when found.
            return MemberFieldMappings.FirstOrDefault(map => map.Name.EqualsIgnoreCase(name, false))?.Instance;
        }

        #endregion


        #region Enum Overrides

        /// <inheritdoc cref="Enum.Equals(object)" />
        public override bool Equals([CanBeNull] object obj)
        {
            if (obj != null) {
                switch (obj) {
                case TValue value:
                    return Value.Equals(value);

                case TDerived derived:
                    return Value.Equals(derived.Value);
                }
            }

            return false;
        }

        /// <summary>
        ///     The hash code of an enumeration member equals the hash code of the represented value.
        /// </summary>
        /// <returns>The hash code of the represented value.</returns>
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        /// <summary>
        ///     Returns the name of the field represented by the enum member.
        /// </summary>
        /// <returns>Name of field represented by the enum member.</returns>
        [NotNull]
        public override string ToString()
        {
            return Name;
        }

        #endregion


        #region Conversion

        /// <summary>
        ///     Retrieves the enumeration member for a given value.
        /// </summary>
        /// <param name="value">Value to implicitly get enumeration member for.</param>
        /// <returns>Enumeration member instance on success, otherwise <c>null</c>.</returns>
        [CanBeNull]
        public static implicit operator GenericEnum<TValue, TDerived>(TValue value)
        {
            return Members[value];
        }

        /// <summary>
        ///     Yields the value represented by the enumeration member.
        /// </summary>
        /// <param name="member">Enumeration member to get value from.</param>
        /// <returns>Value of enumeration member.</returns>
        public static implicit operator TValue([CanBeNull] GenericEnum<TValue, TDerived> member)
        {
            if (member == null) {
                return default(TValue);
            }

            return member.Value;
        }

        /// <summary>
        ///     Tries to get the enumeration member with the specified value.
        /// </summary>
        /// <param name="value">Value to search for.</param>
        /// <param name="member">
        ///     Receives the found enumeration member instance on success, or, if not found, the default for the
        ///     enumeration type.
        /// </param>
        /// <returns><c>true</c>, if a member with the given member was found, otherwise <c>false</c>.</returns>
        public static bool TryConvert(TValue value, out TDerived member)
        {
            if (Members.Count > 0) {
                return Members.TryGetValue(value, out member);
            }

            // Not yet initialised. Lookup manually from mapping.
            var result = MemberFieldMappings.FirstOrDefault(
                memberMapping => {
                    var instance = memberMapping.Instance;

                    Debug.Assert(
                        instance != null,
                        "{0}.{1} != null".FormatWith(nameof(memberMapping), nameof(memberMapping.Instance))
                    );

                    return instance.Value.Equals(value);
                });

            if (result != null) {
                member = result.Instance;
                return true;
            }

            member = default(TDerived);
            return false;
        }

        #endregion


        #region IComparable

        /// <inheritdoc />
        public int CompareTo(object obj)
        {
            if (obj != null) {
                switch (obj) {
                case TValue value:
                    return Value.CompareTo(value);

                case TDerived derived:
                    return Value.CompareTo(derived.Value);
                }
            }

            return -1;
        }

        #endregion


        #region IComparable<TDerived>

        /// <inheritdoc />
        public int CompareTo(TDerived other)
        {
            if (other == null) {
                return 1;
            }

            return Value.CompareTo(other.Value);
        }

        #endregion


        #region IComparer<TDerived>

        /// <inheritdoc />
        public int Compare(TDerived x, TDerived y)
        {
            if (x == null) {
                return -1;
            }

            if (y == null) {
                return 1;
            }

            return x.Value.CompareTo(y.Value);
        }

        #endregion


        #region IEquatable<TDerived>

        /// <inheritdoc />
        /// <remarks>Two instances are considered equal if their values are equal.</remarks>
        public bool Equals(TDerived other)
        {
            if (other == null) {
                return false;
            }

            return Value.Equals(other.Value);
        }

        #endregion


        #region Internal Enum Implementation

        // Utilises mechanisms and workarounds from:
        // http://stackoverflow.com/questions/261663/can-we-define-implicit-conversions-of-enums-in-c

        /// <summary>
        ///     List of enumeration values with their associated instances. These are automatically collected during the
        ///     instantiation of enumeration members.
        /// </summary>
        /// <remarks>
        ///     Members may not have been initialised yet if a static method such as <see cref="Parse" /> is being
        ///     called. This collection is used for implicit conversion of existing members or for efficiently parsing
        ///     a value (with fallback to manually checking member mappings if value list is empty).
        /// </remarks>
        [NotNull]
        private static readonly SortedList<TValue, TDerived> Members = new SortedList<TValue, TDerived>();

        /// <summary>
        ///     Instance description. Lazily retrieves the description attribute value via reflection on first access.
        /// </summary>
        [NotNull]
        private readonly Lazy<string> _description;

        /// <summary>
        ///     Instance name, lazily retrieves the field name via reflection on first access.
        /// </summary>
        [NotNull]
        private readonly Lazy<string> _name;

        /// <summary>
        ///     Statically initialised mapping of all member fields that are static and public. A mapping is lazily
        ///     resolved once the corresponding field properties are accessed, which itself will lazily resolve the
        ///     requested field name or description attribute value.
        /// </summary>
        [NotNull]
        [ItemNotNull]
        private static readonly IReadOnlyCollection<MemberFieldInfo> MemberFieldMappings =
            new ReadOnlyCollection<MemberFieldInfo>(
                typeof(TDerived).GetFields(BindingFlags.Public | BindingFlags.Static)
                                ?.Where(field => field != null && field.FieldType == typeof(TDerived))
                                .Select(field => new MemberFieldInfo(field))
                                .ToList()
                ?? new List<MemberFieldInfo>(0)
            );

        /// <summary>
        ///     Wraps a <see cref="FieldInfo" /> instance retrieved during static initialisation and provides an 
        ///     interface to lazily retrieve the requested enumeration properties via reflection.
        /// </summary>
        private sealed class MemberFieldInfo
        {

            /// <summary>
            ///     The represented member field.
            /// </summary>
            [NotNull]
            private readonly FieldInfo _fieldInfo;

            /// <summary>
            ///     The enumeration member instance that is Lazily retrieved on first access.
            /// </summary>
            [NotNull]
            private readonly Lazy<TDerived> _lazyInstance;

            /// <summary>
            ///     The description attribute value of the field. Accessed lazily when the corresponding property is
            ///     accessed.
            /// </summary>
            [CanBeNull]
            internal string Description => _fieldInfo.GetCustomAttribute<DescriptionAttribute>()?.Description;

            /// <summary>
            ///     The enum member instance.
            /// </summary>
            [NotNull]
            internal TDerived Instance
            {
                get {
                    var value = _lazyInstance.Value;

                    if (value == null) {
                        throw new RuntimeInconsistencyException(
                            "{0} property of {1} wrapper returned null.".FormatWith(
                                nameof(_lazyInstance.Value),
                                _lazyInstance.GetType()
                            )
                        );
                    }

                    return value;
                }
            }

            /// <summary>
            ///     The name of the field. Accessed lazily when the corresponding property is accessed.
            /// </summary>
            [NotNull]
            internal string Name
            {
                get {
                    var name = _fieldInfo.Name;

                    if (name == null) {
                        throw new RuntimeInconsistencyException(
                            "{0} returned null for property {1}.".FormatWith(
                                _fieldInfo.GetType(),
                                nameof(_fieldInfo.Name)
                            )
                        );
                    }

                    return name;
                }
            }

            /// <summary>
            ///     Initialises a new member wrapper instance.
            /// </summary>
            /// <param name="info">The <see cref="FieldInfo" /> instance retrieved during static initialisation.</param>
            internal MemberFieldInfo([NotNull] FieldInfo info)
            {
                Debug.Assert(info != null, $"{nameof(info)} != null");

                _fieldInfo = info;
                _lazyInstance = new Lazy<TDerived>(() => (TDerived) _fieldInfo.GetValue(null));
            }

        }

        #endregion

    }

}
