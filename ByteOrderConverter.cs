﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Runtime.CompilerServices;


namespace XploRe.Runtime
{

    /// <summary>
    ///     Byte-order conversion routines.
    /// </summary>
    public static class ByteOrderConverter
    {

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to the requested byte-order.
        /// </summary>
        /// <param name="order">Requested byte-order to convert <paramref name="value" /> to.</param>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to the requested byte-order.</returns>
        public static short HostTo(ByteOrder order, short value)
        {
            if (order.IsHostOrder()) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to the requested byte-order.
        /// </summary>
        /// <param name="order">Requested byte-order to convert <paramref name="value" /> to.</param>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to the requested byte-order.</returns>
        public static ushort HostTo(ByteOrder order, ushort value)
        {
            if (order.IsHostOrder()) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to the requested byte-order.
        /// </summary>
        /// <param name="order">Requested byte-order to convert <paramref name="value" /> to.</param>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to the requested byte-order.</returns>
        public static int HostTo(ByteOrder order, int value)
        {
            if (order.IsHostOrder()) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to the requested byte-order.
        /// </summary>
        /// <param name="order">Requested byte-order to convert <paramref name="value" /> to.</param>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to the requested byte-order.</returns>
        public static uint HostTo(ByteOrder order, uint value)
        {
            if (order.IsHostOrder()) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to the requested byte-order.
        /// </summary>
        /// <param name="order">Requested byte-order to convert <paramref name="value" /> to.</param>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to the requested byte-order.</returns>
        public static long HostTo(ByteOrder order, long value)
        {
            if (order.IsHostOrder()) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to the requested byte-order.
        /// </summary>
        /// <param name="order">Requested byte-order to convert <paramref name="value" /> to.</param>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to the requested byte-order.</returns>
        public static ulong HostTo(ByteOrder order, ulong value)
        {
            if (order.IsHostOrder()) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to big-endian byte-order.
        /// </summary>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to big-endian byte-order.</returns>
        public static short HostToBigEndian(short value)
        {
            if (BitConverter.IsLittleEndian) {
                return Swap(value);
            }

            return value;
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to big-endian byte-order.
        /// </summary>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to big-endian byte-order.</returns>
        public static ushort HostToBigEndian(ushort value)
        {
            if (BitConverter.IsLittleEndian) {
                return Swap(value);
            }

            return value;
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to big-endian byte-order.
        /// </summary>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to big-endian byte-order.</returns>
        public static int HostToBigEndian(int value)
        {
            if (BitConverter.IsLittleEndian) {
                return Swap(value);
            }

            return value;
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to big-endian byte-order.
        /// </summary>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to big-endian byte-order.</returns>
        public static uint HostToBigEndian(uint value)
        {
            if (BitConverter.IsLittleEndian) {
                return Swap(value);
            }

            return value;
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to big-endian byte-order.
        /// </summary>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to big-endian byte-order.</returns>
        public static long HostToBigEndian(long value)
        {
            if (BitConverter.IsLittleEndian) {
                return Swap(value);
            }

            return value;
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to big-endian byte-order.
        /// </summary>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to big-endian byte-order.</returns>
        public static ulong HostToBigEndian(ulong value)
        {
            if (BitConverter.IsLittleEndian) {
                return Swap(value);
            }

            return value;
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to little-endian byte-order.
        /// </summary>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to little-endian byte-order.</returns>
        public static short HostToLittleEndian(short value)
        {
            if (BitConverter.IsLittleEndian) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to little-endian byte-order.
        /// </summary>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to little-endian byte-order.</returns>
        public static ushort HostToLittleEndian(ushort value)
        {
            if (BitConverter.IsLittleEndian) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to little-endian byte-order.
        /// </summary>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to little-endian byte-order.</returns>
        public static int HostToLittleEndian(int value)
        {
            if (BitConverter.IsLittleEndian) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to little-endian byte-order.
        /// </summary>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to little-endian byte-order.</returns>
        public static uint HostToLittleEndian(uint value)
        {
            if (BitConverter.IsLittleEndian) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to little-endian byte-order.
        /// </summary>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to little-endian byte-order.</returns>
        public static long HostToLittleEndian(long value)
        {
            if (BitConverter.IsLittleEndian) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value from host byte-order to little-endian byte-order.
        /// </summary>
        /// <param name="value">The value to convert in host byte-order.</param>
        /// <returns>Value converted to little-endian byte-order.</returns>
        public static ulong HostToLittleEndian(ulong value)
        {
            if (BitConverter.IsLittleEndian) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Swaps the byte-order of the provided value.
        /// </summary>
        /// <param name="value">Value to swap byte-order of.</param>
        /// <returns>Value with swapped byte-order.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static short Swap(short value)
        {
            return unchecked ((short) ((((ushort) value & 0x00ff) << 8) | (((ushort) value & 0xff00) >> 8)));
        }

        /// <summary>
        ///     Swaps the byte-order of the provided value.
        /// </summary>
        /// <param name="value">Value to swap byte-order of.</param>
        /// <returns>Value with swapped byte-order.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ushort Swap(ushort value)
        {
            return unchecked ((ushort) ((value << 8) | (value >> 8)));
        }

        /// <summary>
        ///     Swaps the byte-order of the provided value.
        /// </summary>
        /// <param name="value">Value to swap byte-order of.</param>
        /// <returns>Value with swapped byte-order.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int Swap(int value)
        {
            // Swap adjacent 16-bit blocks.
            value = unchecked((int) ((value & 0xffff0000) >> 16) | ((value & 0x0000ffff) << 16));

            // Swap adjacent 8-bit blocks.
            return unchecked((int) ((value & 0xff00ff00) >> 8) | ((value & 0x00ff00ff) << 8));
        }

        /// <summary>
        ///     Swaps the byte-order of the provided value.
        /// </summary>
        /// <param name="value">Value to swap byte-order of.</param>
        /// <returns>Value with swapped byte-order.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint Swap(uint value)
        {
            // Swap adjacent 16-bit blocks.
            value = (value >> 16) | (value << 16);

            // Swap adjacent 8-bit blocks.
            return ((value & 0xff00ff00) >> 8) | ((value & 0x00ff00ff) << 8);
        }

        /// <summary>
        ///     Swaps the byte-order of the provided value.
        /// </summary>
        /// <param name="value">Value to swap byte-order of.</param>
        /// <returns>Value with swapped byte-order.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static long Swap(long value)
        {
            return unchecked((long) Swap((ulong) value));
        }

        /// <summary>
        ///     Swaps the byte-order of the provided value.
        /// </summary>
        /// <param name="value">Value to swap byte-order of.</param>
        /// <returns>Value with swapped byte-order.</returns>
        public static ulong Swap(ulong value)
        {
            // Swap adjacent 32-bit blocks.
            value = (value >> 32) | (value << 32);

            // Swap adjacent 16-bit blocks.
            value = (value & 0xffff0000ffff0000) >> 16 | ((value & 0x0000ffff0000ffff) << 16);

            // Swap adjacent 8-bit blocks.
            return (value & 0xff00ff00ff00ff00) >> 8 | ((value & 0x00ff00ff00ff00ff) << 8);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value in the given byte-order to host byte-order.
        /// </summary>
        /// <param name="order">Byte-order of <paramref name="value" /> to convert from.</param>
        /// <param name="value">The value to convert in the given byte-order.</param>
        /// <returns>Value converted to host byte-order.</returns>
        public static short ToHostFrom(ByteOrder order, short value)
        {
            if (order.IsHostOrder()) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value in the given byte-order to host byte-order.
        /// </summary>
        /// <param name="order">Byte-order of <paramref name="value" /> to convert from.</param>
        /// <param name="value">The value to convert in the given byte-order.</param>
        /// <returns>Value converted to host byte-order.</returns>
        public static ushort ToHostFrom(ByteOrder order, ushort value)
        {
            if (order.IsHostOrder()) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value in the given byte-order to host byte-order.
        /// </summary>
        /// <param name="order">Byte-order of <paramref name="value" /> to convert from.</param>
        /// <param name="value">The value to convert in the given byte-order.</param>
        /// <returns>Value converted to host byte-order.</returns>
        public static int ToHostFrom(ByteOrder order, int value)
        {
            if (order.IsHostOrder()) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value in the given byte-order to host byte-order.
        /// </summary>
        /// <param name="order">Byte-order of <paramref name="value" /> to convert from.</param>
        /// <param name="value">The value to convert in the given byte-order.</param>
        /// <returns>Value converted to host byte-order.</returns>
        public static uint ToHostFrom(ByteOrder order, uint value)
        {
            if (order.IsHostOrder()) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value in the given byte-order to host byte-order.
        /// </summary>
        /// <param name="order">Byte-order of <paramref name="value" /> to convert from.</param>
        /// <param name="value">The value to convert in the given byte-order.</param>
        /// <returns>Value converted to host byte-order.</returns>
        public static long ToHostFrom(ByteOrder order, long value)
        {
            if (order.IsHostOrder()) {
                return value;
            }

            return Swap(value);
        }

        /// <summary>
        ///     Converts the byte-order of the provided value in the given byte-order to host byte-order.
        /// </summary>
        /// <param name="order">Byte-order of <paramref name="value" /> to convert from.</param>
        /// <param name="value">The value to convert in the given byte-order.</param>
        /// <returns>Value converted to host byte-order.</returns>
        public static ulong ToHostFrom(ByteOrder order, ulong value)
        {
            if (order.IsHostOrder()) {
                return value;
            }

            return Swap(value);
        }

    }

}
